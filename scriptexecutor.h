#ifndef SCRIPTEXECUTOR_H
#define SCRIPTEXECUTOR_H

#include <QObject>
#include <QProcess>


/**
 * @brief Qt class, designed for execute Bash scripts
 */
class ScriptExecutor : public QObject
{
    
    /**
     * @brief standard preprocesor directive for Qt class
     */
    Q_OBJECT
public:

    /**
     * @brief Construct a new Script Executor object
     * 
     * @param args private QString variable for store executed program script
     * @param executorPath private QString variable for store executed program parth
     * @param parent parent QtObject, can be skipped (default is nullptr)
     */
    explicit ScriptExecutor(QString args, QString executorPath, QObject *parent = nullptr);

    
    /**
     * @brief Destroy the Script Executor object,
     * invoke deleteLater() method for QProces pionter, created in constructor.
     */
    ~ScriptExecutor();


    /**
     * @brief method for execute, script
     * @property Q_INVOKABLE, mandatory for Qml invocation 
     * @return QString of standard output from QProcess
     */
    Q_INVOKABLE QString output();


    /**
     * @brief public QString pointer, initialized by QByteArray returned from executed process,
     */
    QString *out;

private:
    /**
     * @brief private QProcess pointer, initialized in constructor.
     */
    QProcess *p;

    /**
     * @brief private QString variable for store executed program script,
     * it's set up in constructor. 
     */
    const QStringList _args;

    /**
     * @brief private QString variable for store executed program parth,
     * it's set up in constructor. 
     */
    const QString _executorPath;
};

#endif // SCRIPTEXECUTOR_H
