#include "scriptexecutor.h"

ScriptExecutor::ScriptExecutor(QString executorPath,QString args, QObject *parent)
    : QObject(parent)
    , _args(args)
    , _executorPath(executorPath)
{
    p = new QProcess (this);
}

QString ScriptExecutor::output()
{
    p->start(_executorPath,QStringList() << this->_args);
    p->waitForFinished(200);
    out = new QString(p->readAllStandardOutput());
    p->close();
    return *out;
}

ScriptExecutor::~ScriptExecutor()
{
    p->deleteLater();
}
