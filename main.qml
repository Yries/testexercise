import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.3

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Grid {
        id: grid
        x: 0
        y: 0
        width: 640
        height: 480
    }

    Rectangle {
        id: rectangle
        x: 119
        y: 289
        width: 402
        height: 93
        color: "#ffffff"
        border.color: "#000000"

        Text {
            id: textToShow
            x: 0
            y: 0
            text: qsTr("Hello, 3D lab")
            width: 402
            height: 93
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 12
        }
    }

    Button {
        id: button
        x: 204
        y: 104
        width: 232
        height: 83
        text: qsTr("Click Me !!!")
        focusPolicy: Qt.NoFocus
    }

    Connections {
        target: button
        onClicked: {
            var ips = srcExec.output()
            textToShow.text = qsTr(ips)
            console.log("Result of script: ", ips)
        }


    }
}
