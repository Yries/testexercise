#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "scriptexecutor.h"
#include <QDebug>

/**
 * @brief 
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char *argv[])
{
    qDebug() << argv[0];

    /**
     * @brief Default construct a new QCoreApplication::setAttribute object
     */
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
   
    /**
     * @brief Default initializaction of QGuiApplication
     * @return QGuiApplication, initialized by program parameters
     */
    QGuiApplication app(argc, argv);

    /**
     * @brief initialization of ScriptExecutor class initialized with parameters
     * @param executorPath path of program to execute
     * @param args arguments, in this case - just patch of script.
     * It can be given like "/home/user/TestExercise/getIP.sh", or
     * by default execution path, given by default by standard parameter - argv[0]
     */
    ScriptExecutor *srcExec = new ScriptExecutor("/bin/bash", QString(argv[0]) + "/getIP.sh");

    /**
     * @brief Default initialization of Qml App engine
     */
    QQmlApplicationEngine engine;

    /**
     * @brief Default initialization of Qml App
     */
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    /**
     * @brief Default protection of Qml App engine, Added int newer versions of Qt
     */
    if (engine.rootObjects().isEmpty())
        return -1;

    /**
     * @brief registration of ScriptExecutor in engine, necessary
     */
    engine.rootContext()->setContextProperty("srcExec",srcExec);


    /**
     * @brief 
     */
    return app.exec();
}
